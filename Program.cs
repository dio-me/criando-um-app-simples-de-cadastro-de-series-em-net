﻿using SysEnum = System.Enum;
using DIO.Series.Enum;

namespace DIO.Series
{
    class Program
    {
        static SerieRepositorio repositorio = new SerieRepositorio();

        static void Main(string[] args)
        {

            string opcaoUsuario = ObterOpcaoUsuario();

            while (opcaoUsuario.ToUpper() != "X")
            {
                switch (opcaoUsuario)
                {
                    case "1":
                        ListarSeries();
                        break;
                    case "2":
                        InserirSeries();
                        break;
                    case "3":
                        AtualizarSerie();
                        break;
                    case "4":
                        ExcluirSeries();
                        break;
                    case "5":
                        VisualizarSerie();
                        break;
                    case "C":
                        Console.Clear();
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
                opcaoUsuario = ObterOpcaoUsuario();
            }
            Console.WriteLine("Obrigado por utilizar nossos serviços.");
            Console.ReadLine();
        }

        private static void VisualizarSerie()
        {
             System.Console.WriteLine("Digite o ID da serie");
            int indiceSerie = int.Parse(Console.ReadLine());

            var serie = repositorio.RetornaPorId(indiceSerie);
            System.Console.WriteLine(serie);
        }

        private static void ExcluirSeries()
        {
            System.Console.WriteLine("Digite o ID da serie");
            int indiceSerie = int.Parse(Console.ReadLine());

            repositorio.Excluir(indiceSerie);
        }

        private static void AtualizarSerie()
        {
            System.Console.WriteLine("Digite o ID da serie");
            int indiceSerie = int.Parse(Console.ReadLine());

            foreach (int i in SysEnum.GetValues(typeof(Genero)))
            {
                System.Console.WriteLine("{0}-{1}", i, SysEnum.GetName(typeof(Genero), i));
            }
            System.Console.WriteLine("Digite o genero entre as opçoesacima:");
            int entradagenero = int.Parse(Console.ReadLine());

            System.Console.WriteLine("Digite o Titulo da serie:");
            string entradaTitulo = Console.ReadLine();


            System.Console.WriteLine("Digite o Ano da serie:");
            int entradaAno = int.Parse(Console.ReadLine());

            System.Console.WriteLine("Digite o Descrição da serie:");
            string entradaDescricao = Console.ReadLine();

            Serie atualizarSerie = new Serie(id: indiceSerie,
            genero: (Genero)entradagenero,
            titulo: entradaTitulo,
            ano: entradaAno,
            descricao: entradaDescricao);

            repositorio.Atualiza(indiceSerie, atualizarSerie);

        }
        private static void InserirSeries()
        {
            System.Console.WriteLine("Inserir nova serie");
            foreach (int i in SysEnum.GetValues(typeof(Genero)))
            {
                System.Console.WriteLine("{0}-{1}", i, SysEnum.GetName(typeof(Genero), i));
            }

            System.Console.WriteLine("Digite o genero entre as opçoesacima:");
            int entradagenero = int.Parse(Console.ReadLine());

            System.Console.WriteLine("Digite o Titulo da serie:");
            string entradaTitulo = Console.ReadLine();

            System.Console.WriteLine("Digite o Ano da serie:");
            int entradaAno = int.Parse(Console.ReadLine());

            System.Console.WriteLine("Digite o Descrição da serie:");
            string entradaDescricao = Console.ReadLine();

            Serie novaSerie = new Serie(id: repositorio.ProximoId(),
            genero: (Genero)entradagenero,
            titulo: entradaTitulo,
            ano: entradaAno,
            descricao: entradaDescricao);
            repositorio.Insere(novaSerie);
        }

        private static void ListarSeries()
        {
            System.Console.WriteLine("Listar Series");
            var lista = repositorio.Lista();

            if (lista.Count == 0)
            {
                System.Console.WriteLine("Nenhuma serie cadastrada.");
                return;
            }

            foreach (var serie in lista)
            {
                var excluido = serie.retornaExcluido();
                System.Console.WriteLine($"#ID {serie.retornaId()}, {serie.retornaTitulo()}, {(excluido ? "*Excluido*" : "")}");
            }
        }

        private static string ObterOpcaoUsuario()
        {
            System.Console.WriteLine();
            System.Console.WriteLine("Dio Series  sei dispor !!!");
            System.Console.WriteLine("Informe a opção desejada");

            System.Console.WriteLine("1 - Lista series");
            System.Console.WriteLine("2 - Inserir nova série");
            System.Console.WriteLine("3 - Atualizar série");
            System.Console.WriteLine("4 - Excluir Série");
            System.Console.WriteLine("5 - Visualizar Série");
            System.Console.WriteLine("C - Limpar Tela");
            System.Console.WriteLine("X - Sair");
            System.Console.WriteLine();

            string opcaosuario = Console.ReadLine().ToUpper();
            System.Console.WriteLine();
            return opcaosuario;

        }
    }

}
